#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector(): x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void ShowLengthVector()
	{
		std::cout << "Length vector: " << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << "\n";
	}

private:
	double x;
	double y;
	double z;


};

int main()
{
	double x, y, z;
	std::cout << "Enter x: \n";
	std::cin >> x;
	std::cout << "Enter y: \n";
	std::cin >> y;
	std::cout << "Enter z: \n";
	std::cin >> z;


	Vector v(x, y, z);
	v.ShowLengthVector();
}